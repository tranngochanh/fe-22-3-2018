var array1 = ["Grape", "Apple"];
var array2 = ["Orange", "Pinapple", "Banana"];

var arrayConcat = array1.concat(array2);
console.log(arrayConcat);

var numArray1 = [1, 4, 7, 3, 8, 2, 10, 11, 54, 23, 15, 25, 67];

function modeTwo(element, index, array) {
    return (element % 2 == 0);
}

var filterArray = numArray1.filter(modeTwo);
console.log(filterArray);

function logModeFive(element, index, array) {
    if (element % 5 == 0)
        document.write(element + "<br>");
}

numArray1.forEach(logModeFive);

function powTwo(element, index, array) {
    return element * element;
}

console.log(numArray1.map(powTwo));


var arr = [1, 2, 3, 4, 9, 8, 7, 6, 5];
for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 != 0) {
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[i] > arr[j] && arr[j] %2 !=0) {
                var temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}
console.log(arr);

var arr2 = [1,2,3,4,5,6,7,8,9,10];
function chia3du1(element,index,array){
    return (element%3 == 1);
}
function chia3du2(element,index,array){
    return (element%3 == 2);
}
function chia3du0(element,index,array){
    return(element%3 == 0);
}

var kq1 = arr2.filter(chia3du1);
var kq2 = arr2.filter(chia3du2);
var kq3 = arr2.filter(chia3du0);

var kq = kq1.concat(kq3).concat(kq2);
console.log(kq);

function formatDate(date) {
    return (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate()) 
        + "-" + (date.getMonth() + 1 < 10 ? ("0" + (date.getMonth() + 1)) : (date.getMonth() + 1)) 
        + "-" + date.getFullYear();
}

var date = new Date();
console.log(formatDate(date));

var milisec= date.getTime();
var chrismax = new Date(2018, 11, 25);
var chrismaxmilisec = chrismax.getTime();
var lech= chrismaxmilisec - milisec ;
var remain = lech / (24*60*60*1000);
console.log(remain);

