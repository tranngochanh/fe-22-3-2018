$(document).ready(function() {
    getData();
});

function getData() {
    $.ajax({
        type: "GET",
        url: "https://www.jasonbase.com/things/DG8m.json",
        success: function(response) {
            var data = response.data;
            var html = "";
            for (let i = 0; i < data.length; i++) {
                const element = data[i];
                html += "<a href=\"./detail.html?key=" + element.key + "\"><div class=\"item\">" +
                    "<div>" +
                        "<div class=\"image\">" +
                            "<img src=\" " + element.image + "\" />" +
                        "</div>" +
                        "<h4>" + element.title + "</h4>" +
                        "<p>" + element.message + "</p>" +
                    "</div>" +
                "</div></a>";
            }
            $('.list').prepend(html);
        },
        error: function (error) {
            console.log(error);
        }
    });
}